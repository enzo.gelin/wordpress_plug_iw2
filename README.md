# wordpress_plug_IW2

![](https://img.shields.io/badge/WP-Widget-brightgreen) ![](https://img.shields.io/badge/version-1.0.0-yellow) ![](https://img.shields.io/badge/ESGI-Give%20a%20man%20a%20gun%20and%20he%20can%20rob%20a%20bank.%20Give%20a%20man%20a%20bank%20and%20he%20can%20rob%20the%20world-lightgrey)

# Welcome to our year-end review

Here are the instructions for the end of year exam : 

| Obj. | Det. |
| ------ | ------ |
| Objective of the project| Develop a plugin on Wordpress or Prestashop (your choice) |
| Detailed description | Develop a plugin on Wordpress or Prestashop (of your choice) respecting and using the methods learned during the training weeks. |

## Description

Global :
  - Clever technical approach
  - Smart use of CMS

Coded :
  - readable / well indented / syntax OK
  - concise / without functional redundancy or repetitions
  - appropriate function / variable / file names
  - relevant comments

Documentation (READ ME):
  - Easy to reproduce (clear and complete explanations)
  - Aesthetics / Readability / Well spelled

# FlickrApiEsgi

Display a selected number of photos from flickr with the identifier of your account or that of a friend, you can also change the size of these images for display.


learn more : [fb-widget/README.md][PlFb]

# FacebookAccountEsgi

Display the page of a facebook account of your choice on your WordPress Site simply and quickly

learn more : [flickr-api-widget/README.md][PlFl]


[PlFb]: <https://gitlab.com/enzo.gelin/wordpress_plug_iw2/-/tree/master/fb-widget/README.md>
[PlFl]: <https://gitlab.com/enzo.gelin/wordpress_plug_iw2/-/tree/master/flickr-api-widget/README.md>

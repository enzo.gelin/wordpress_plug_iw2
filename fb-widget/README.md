# wordpress_plug_IW2

![](https://img.shields.io/badge/WP-Facebook-blue) ![](https://img.shields.io/badge/version-1.0.0-yellow)

# FacebookAccountEsgi

Requires PHP: 7.2.*

Requires : Wordpress (lmao)

Display the page of a facebook account of your choice on your WordPress Site simply and quickly

## Description

Display the page of a facebook account quickly and simply with the choice of style and responsive.

## Installation

1# Clone Repo

2# Open your wordpress admin panel

3# On the left menu, go to \"Plugin\" then \"add new\"

4# At the top of the page, click on \"Upload plugin\"

5# Choose the compressed file (fb-widget.zip) and click on \"Install now\" then \"Activate Plugin\"

6# You should see the plugin \"T'as vue ma page Facebook?\" in the list now

## Integration

1# Below the \"T'as vue ma page Facebook?\" line, click on Settings (\"Paramètres\")

2# Normally you will be redirected to a setting page where you can configure the widget

3# Configure and enjoy ;)

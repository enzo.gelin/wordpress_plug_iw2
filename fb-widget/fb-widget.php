<?php

/*
Plugin Name: T'as vue ma page Facebook?
Description: Afficher, sur son Wordpress Site & dans un encard, la page facebook de votre choix simplement
Plugin URI: https://esgi.nador/
AUthor: EJ
Author URI: https://esgi.tripoli/
Version: 1.0.0
*/

defined('ABSPATH') or die('ERREUR : ABSPATH');

// Tableau de parametre du widget
$widget_params = array(
    array(
        'id' => 'hidden_on_mobile',
        'type' => 'radio',
        'default' => '1',
        'label' => 'Cacher sur les smartphones et tablettes',
        'desc' => 'Cacher la page Facebook sur les appareils de type smartphones et tablettes',
        'options' => array(
            0 => 'Non',
            1 => 'Oui'
        )
    ),
    array(
        'id' => 'border-radius',
        'type' => 'radio',
        'default' => '1',
        'label' => 'Arrondir les bords de l\'élément',
        'desc' => 'Changer le style des éléments en rajoutant un effet arrondi sur les coins',
        'options' => array(
            0 => 'Non',
            1 => 'Oui'
        )
    ),
    array(
        'id' => 'facebook_id',
        'type' => 'text',
        'default' => 'facebook',
        'label' => 'Facebook ID',
        'desc' => 'Mon Facebook ID'
    ),
    array(
        'id' => 'fa-cdn',
        'type' => 'radio',
        'default' => '0',
        'label' => 'Afficher le logo Facebook',
        'desc' => 'Cliquez sur Oui si vous voulez afficher l\icône de Facebook (fontawesome)',
        'options' => array(
            0 => 'Non',
            1 => 'Oui'
        )
    )
);

add_action('wp_head', 'widgetScripts');
add_action('wp_enqueue_scripts', 'widgetScripts');

// Function charge script + css fontawesome si demandé (facebook icon)
function widgetScripts()
{
    wp_enqueue_style('style', plugin_dir_url(__FILE__) . 'css/style.css');
    if (trim(get_option('fa-cdn')) == 1) {
        wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    }
}
/* Shortcodes */

//[fb]
add_shortcode('fb', 'widgetFrontend');

add_action('wp_footer', 'widgetFrontend');

// Function affichae en front 
function widgetFrontend()
{
    if (trim(get_option('hidden_on_mobile')) == 0) { ?>
        <div class="social_mobile">
            <div class="top-left">
                <?php
                $sum = 0;
                if (!empty(get_option('facebook_id'))) {
                    $sum++;
                    // Verif si IOs ou Android
                    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
                    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
                    $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
                    // Build url fb specific
                    if ($iPhone || $iPad) {
                        $fb_url = 'fb://profile/' . get_option('facebook_id');
                    } else if ($Android) {
                        $fb_url = 'fb://page/' . get_option('facebook_id');
                    }
                ?>
                    <a class="facebook" href="<?php echo $fb_url ?>" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="social_slider" style="top: 25vh;">
        <?php if (!empty(get_option('facebook_id'))) { ?>
            <input id="tabOne" type="radio" name="tabs" checked />
            <label for="tabOne" class="facebook_icon" style="max-width: 32px;"><span>facebook</span><i class="fa fa-facebook-square"></i></label>
            <section id="contentOne">
                <div class="facebook_box">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/<?php echo get_option('facebook_id'); ?>&tabs=timeline,events,messages&width=350&height=490&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true" width="350" height="490" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
                    </iframe>
                </div>
            </section>
        <?php } ?>
    </div>
    <style>
        <?php if (trim(get_option('hidden_on_mobile')) == 0) { ?>
        .social_mobile a,
        .social_mobile a:focus,
        .social_mobile a:hover {
            width: calc(100% / <?php echo $sum ?>);
        }

        <?php } if (trim(get_option('border-radius')) == 1) { ?>
        .social_slider .facebook_icon,
        .social_slider .twitter_icon {
            border-radius: 0 5px 5px 0 !important;
        }
        .social_slider .facebook_box {
            border-radius: 8px!important; 
        }

        <?php } ?>
    </style>
<?php }

// Setting page dans l'administration ( elle sert à definir les infos de base du wiget tel que l'id FB ou encore la mise ne forme du wigdet)
add_action('admin_menu', 'widgetMenu');

function widgetMenu()
{
    add_options_page('T\'as vue ma page Facebook?', 'Paramètres Facebook Widget', 'manage_options', 'account-facebook-widget', 'widgetBackend');
}

// Function bouton dans "Plugin gestion"
function filter_action_links($links)
{
    $links['settings'] = '<a href="' . admin_url('/options-general.php?page=account-facebook-widget') . '">' . __('Paramètres') . '</a>';
    $links['support'] = '<a href="https://www.esgi.fr/" target="_blank">' . __('ESGI') . '</a>';
    return $links;
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'filter_action_links', 10, 2);

// Function Afichage parametre dans setting administration
function widgetBackend()
{
    global $widget_params;
    if ($_POST) {
        foreach ($widget_params as $param) {
            $save_param = sanitize_text_field($_POST[$param['id']]);
            update_option($param['id'], $save_param);
        }
        echo '<div class="updated fade"><p><strong>' . __('Modificications enregistrées avec succès.') . '</strong></p></div>';
    }

    echo '<form method="post">';
    settings_fields('setting-fields');
    do_settings_sections('account-facebook-widget');
    echo '<h1>' . get_admin_page_title() . '</h1><br></br><h3>Cliquez ici si vous connaissez pas votre Facebook ID: <a href="https://findmyfbid.com/" target="_blank">Retrouver mon FB ID</a></h3><hr><table class="form-table">';
    foreach ($widget_params as $param) {
        echo '<tr valign="top"><th scope="row" style="width: 30%;">' . $param['label'] . '</th><td>';
        switch ($param['type']) {
            case 'text':
                echo '<input type="text" name="' . $param['id'] . '" value="' . get_option($param['id']) . '" style="width: 35%;" />';
                break;
            case 'radio':
                echo '<select name="' . $param['id'] . '" style="width: 100%;">';
                foreach ($param['options'] as $optionn => $optionv) {
                    echo '<option value="' . $optionn . '" ' . (($optionn == get_option($param['id'])) ? ' selected="selected"' : '') . '>' . $optionv . '</option>';
                }
                echo '</select>';
                break;
        }
        echo '</td></tr>';
    }

    echo '</table>
            <p class="submit">
                <input type="submit" class="button-secondary" value="' . __('Enregistrer les modifications') . '" />
            </p>
	    </form>
    </div>';
}

?>
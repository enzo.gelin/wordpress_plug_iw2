<?php

/* Test */
add_action('wp_footer', 'myplugin_FooterAddText');

function myplugin_FooterAddText() {
	echo "<i>Mon plugin TOTO est activé!</i>";
}

/* Add Admin Menu page for settings and stuffs */

add_action('admin_menu', 'myplugin_AddAdminMenu');

function myplugin_AddAdminMenu() {
	add_menu_page(
		'Plugin Toto Home',
		'Plugin Toto',
		'manage_options',
		'monplugin/includes/toto-acp.php'
	);
}

/* Shortcodes */

//[toto]
add_shortcode('toto', 'myplugin_TotoShortcode');

function myplugin_TotoShortcode() {
	return "<i>mon shortcode toto</i>";
}


//[titi att="value"]
add_shortcode('titi', 'myplugin_TitiShortcode');

function myplugin_TitiShortcode($atts) {
	$a = shortcode_atts(array(
		'x' => 'un truc',
		'y' => 'un autre'
	), $atts);

	return "<p>x = {$a['x']} puis y = {$a['y']}</p>";
}

/* Widget */

class Toto_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'toto_widget', 
			'Widget Toto', 
			array(
				'description' => __('Widget simple issu du plugin Toto', 'mypluginlg')
			)
		);
	}

	public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        //echo 'Widget Toto';
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $before_widget;
        if ( !empty($title))
        	echo $before_title . $title . $after_title;
        echo $after_widget;
    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
        $title= isset($instance['title']) ? $instance['title'] : '';
        echo'<label for="'.$this->get_field_name('title').'">Title : </label>
        <input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'">';
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags( $new_instance['title']) : '';
        return $instance;
    }

}

add_action('widgets_init', 'myplugin_TotoWidget');

function myplugin_TotoWidget() {
	register_widget('Toto_Widget');
}
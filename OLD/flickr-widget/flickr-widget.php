<?php

/*
	Plugin Name: Flickr API Widget
	Description:  Display recent Flickr images with API
	Plugin URI: https://esgi.nador/
	AUthor: EJ
	Author URI: https://esgi.tripoli/
	Version: 1.0.0
*/

defined('ABSPATH') or die('ERREUR : ABSPATH');

// Tableau de parametre du widget
$widget_params = array(
    array(
        'id' => 'title_widg',
        'type' => 'text',
        'default' => 'title',
        'label' => 'Le titre de votre widget',
        'desc' => 'Mon title ID'
    ),
    array(
        'id' => 'flickr_id',
        'type' => 'text',
        'default' => 'flickr',
        'label' => 'Flickr ID',
        'desc' => 'Mon Flickr ID'
    ),
    array(
        'id' => 'size_img',
        'type' => 'number',
        'default' => '100',
        'label' => 'Taille de l\'image',
        'desc' => 'Gérer la taille de l\'affichage'
    ),
    array(
        'id' => 'border-radius',
        'type' => 'radio',
        'default' => '1',
        'label' => 'Arrondir les bords de l\'élément',
        'desc' => 'Changer le style des éléments en rajoutant un effet arrondi sur les coins',
        'options' => array(
            0 => 'Non',
            1 => 'Oui'
        )
    )
);

add_action('wp_head', 'widgetScriptsBis');
add_action('wp_enqueue_scripts', 'widgetScriptsBis');

// Function charge script + css fontawesome si demandé (facebook icon)
function widgetScriptsBis()
{
    wp_enqueue_style('style', plugin_dir_url(__FILE__) . 'css/style.css');
}

add_action('wp_footer', 'widgetFrontendBis');

// Function affichae en front 
function widgetFrontendBis()
{
?>
        
    <div class="social_slider" style="top: 25vh;">
        <?php if (!empty(get_option('flickr_id'))) { ?>
            <input id="tabOne" type="radio" name="tabs" checked />
            <label for="tabOne" class="facebook_icon" style="max-width: 32px;"><span>facebook</span><i class="fa fa-facebook-square"></i></label>
            <section id="contentOne">
                <div class="facebook_box">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/<?php echo get_option('flickr_id'); ?>&tabs=timeline,events,messages&width=350&height=490&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true" width="350" height="490" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
                    </iframe>
                </div>
            </section>
        <?php } ?>
    </div>
    <style>
        <?php if (trim(get_option('border-radius')) == 1) { ?>
        .social_slider .facebook_icon,
        .social_slider .twitter_icon {
            border-radius: 0 5px 5px 0 !important;
        }
        .social_slider .facebook_box {
            border-radius: 8px!important; 
        }

        <?php } ?>
    </style>
<?php  }

// Setting page dans l'administration ( elle sert à definir les infos de base du wiget tel que l'id FB ou encore la mise ne forme du wigdet)
add_action('admin_menu', 'widgetMenuBis');

function widgetMenuBis()
{
    add_options_page('T\'as vue mes images Flickr?', 'Paramètres Flickr Widget', 'manage_options', 'account-flickr-widget', 'widgetBackendBis');
}

// Function bouton dans "Plugin gestion"
function filter_action_links_Bis($links)
{
    $links['settings'] = '<a href="' . admin_url('/options-general.php?page=account-flickr-widget') . '">' . __('Paramètres') . '</a>';
    $links['support'] = '<a href="https://www.esgi.fr/" target="_blank">' . __('ESGI') . '</a>';
    return $links;
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'filter_action_links_Bis', 10, 2);

// Function Afichage parametre dans setting administration
function widgetBackendBis()
{
    global $widget_params;
    if ($_POST) {
        foreach ($widget_params as $param) {
            $save_param = sanitize_text_field($_POST[$param['id']]);
            update_option($param['id'], $save_param);
        }
        echo '<div class="updated fade"><p><strong>' . __('Modificications enregistrées avec succès.') . '</strong></p></div>';
    }

    echo '<form method="post">';
    settings_fields('setting-fields');
    do_settings_sections('account-flickr-widget');
    echo '<h1>' . get_admin_page_title() . '</h1><br></br><h3>Cliquez ici si vous connaissez pas votre Flickr ID: <a href="https://www.webfx.com/tools/idgettr/" target="_blank">Retrouver mon Flickr ID</a></h3><hr><table class="form-table">';
    foreach ($widget_params as $param) {
        echo '<tr valign="top"><th scope="row" style="width: 30%;">' . $param['label'] . '</th><td>';
        switch ($param['type']) {
            case 'text':
                echo '<input type="text" name="' . $param['id'] . '" value="' . get_option($param['id']) . '" style="width: 35%;" />';
                break;
            case 'number':
                echo '<input type="number" name="' . $param['id'] . '" value="' . get_option($param['id']) . '" style="width: 35%;" />';
                break;
            case 'radio':
                echo '<select name="' . $param['id'] . '" style="width: 100%;">';
                foreach ($param['options'] as $optionn => $optionv) {
                    echo '<option value="' . $optionn . '" ' . (($optionn == get_option($param['id'])) ? ' selected="selected"' : '') . '>' . $optionv . '</option>';
                }
                echo '</select>';
                break;
        }
        echo '</td></tr>';
    }

    echo '</table>
            <p class="submit">
                <input type="submit" class="button-secondary" value="' . __('Enregistrer les modifications') . '" />
            </p>
	    </form>
    </div>';
}

?>
<?php
/*
	Plugin Name: Flickr API Widget
	Description:  Display recent Flickr images with API
	Plugin URI: https://esgi.nador/
	AUthor: EJ
	Author URI: https://esgi.tripoli/
	Version: 1.0.0
*/

/* Security */
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/* Style CSS*/
function register_script()
{
	wp_register_style('widget-flickr', plugins_url('/widget-flickr-api.css', __FILE__));
}

add_action('wp_enqueue_scripts', 'register_script');

add_action('widgets_init', 'register_flickr_widget_api');
function register_flickr_widget_api()
{
	register_widget('WP_Widget_Flickr_API');
}

class  WP_Widget_Flickr_API extends WP_Widget
{

	public function __construct()
	{
		parent::__construct(
			'flickr',
			esc_html__('* Flickr Picture', 'your-text-domain'),
			array('description' => esc_html__('Display photos from flickr wih your account id.', 'your-text-domain'))
		);
	}

	public function widget($args, $instance)
	{

		extract($args);

		$title          = apply_filters('widget_title', $instance['title']);
		$account_id      = (isset($instance['account_id']) && !empty($instance['account_id'])) ? esc_attr($instance['account_id']) : "";
		$photos_number  = (isset($instance['photos_number']) && is_numeric($instance['photos_number'])) ? esc_attr($instance['photos_number']) : 1;
		$width_thumb  = (isset($instance['width_thumb']) && is_numeric($instance['width_thumb'])) ? esc_attr($instance['width_thumb']) : 50;

		echo $before_widget;

		if (!empty($title)) {
			echo $before_title . $title . $after_title;
		}

		if (!empty($account_id)) {

			$api_key = '39c6285740a23ca1296e39a12c851f41';
			$transient_prefix = esc_attr($account_id . $api_key);

			if (false === ($responce_results = get_transient('flickr-widget-' . $transient_prefix))) {

				$url = 'https://www.flickr.com/services/rest/';

				$arguments = array(
					'api_key' => $api_key,
					'method'  => 'flickr.people.getPublicPhotos',
					'format'  => 'json',
					'user_id' => $account_id,
					'per_page' => $photos_number,
				);

				$url_parameters = array();
				foreach ($arguments as $key => $value) {
					$url_parameters[] = $key . '=' . $value;
				}

				$url .= '?' . implode('&', $url_parameters);

				$responce = file_get_contents($url);

				if ($responce) {

					$responce = str_replace('jsonFlickrApi(', '', $responce);
					$responce = str_replace('})', '}', $responce);
					$responce = json_decode($responce, true);

					$responce_results = array();

					if ($responce['stat'] == 'ok') {
						foreach ($responce['photos']['photo'] as $photo) {
							$responce_results[$photo['id']]['link'] = esc_url('//flickr.com/photos/' . $photo["owner"] . '/' . $photo["id"]);
							$responce_results[$photo['id']]['url']  = esc_url('//farm' . $photo["farm"] . '.staticflickr.com/' . $photo["server"] . '/' . $photo["id"] . '_' . $photo["secret"] . '_s.jpg');
							$responce_results[$photo['id']]['alt']  = esc_attr($photo["title"]);
						}

						if (!empty($responce_results)) {
							$responce_results = base64_encode(serialize($responce_results));
							set_transient('flickr-widget-' . $transient_prefix, $responce_results, apply_filters('null_flickr_cache_time', HOUR_IN_SECONDS * 2));
						}
					}
				} else {
					return new WP_Error('flickr_error', esc_html__('Could not get data, sorry...', 'your-text-domain'));
				}
			}

			if (!empty($responce_results)) {
				$responce_results =  unserialize(base64_decode($responce_results));
				wp_enqueue_style('widget-flickr');
				$output = '';
				$output .= '<ul class="widget-flickr-list">';
				foreach ($responce_results as $photo) {
					$output .= '<li>';
					$output .= '<a href="' . $photo['link'] . '" target="_blank">';
					$output .= '<img src="' . $photo['url'] . '" alt="' . $photo['alt'] . '" title="' . $photo['alt'] . '" style="width:' . $width_thumb . 'px;" />';
					$output .= '</a>';
					$output .= '</li>';
				}
				$output .= '</ul>';
				echo $output;
			}
		}

		echo $after_widget;
	}

	public function form($instance)
	{

		$defaults = array(
			'title'          => esc_html__('Flickr Picture', 'your-text-domain'),
			'photos_number'  => '3',
			'account_id'      => '',
			'width_thumb' => '75',
		);

		$instance = wp_parse_args((array) $instance, $defaults);

?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo esc_html__('Titre du widget:', 'your-text-domain'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('account_id'); ?>"><?php echo esc_html__('Flickr Account ID:', 'your-text-domain'); ?> <small>exemple: 67221971@N06</small></label>
			<input class="widefat" id="<?php echo $this->get_field_id('account_id'); ?>" name="<?php echo $this->get_field_name('account_id'); ?>" type="text" value="<?php echo $instance['account_id']; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('photos_number'); ?>"><?php echo esc_html__('Nombre d\'images à afficher:', 'your-text-domain'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('photos_number'); ?>" name="<?php echo $this->get_field_name('photos_number'); ?>" type="text" value="<?php echo $instance['photos_number']; ?>" size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('width_thumb'); ?>"><?php echo esc_html__('Taille (en pixels):', 'your-text-domain'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('width_thumb'); ?>" name="<?php echo $this->get_field_name('width_thumb'); ?>" type="number" min="0" value="<?php echo $instance['width_thumb']; ?>" size="3" />
		</p>
<?php
	}

	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title']          = strip_tags($new_instance['title']);
		$instance['photos_number']  = strip_tags($new_instance['photos_number']);
		$instance['account_id']      = strip_tags($new_instance['account_id']);
		$instance['width_thumb'] = strip_tags($new_instance['width_thumb']);

		$api_key        = '39c6285740a23ca1296e39a12c851f41';
		$transient_name = 'flickr-widget-' . esc_attr($instance['account_id'] . $api_key);

		delete_transient($transient_name);

		return $instance;
	}
}
?>
# wordpress_plug_IW2

![](https://img.shields.io/badge/WP-Flickr-red.svg) ![](https://img.shields.io/badge/version-1.0.0-yellow)

# FlickrApiEsgi

Requires PHP: 7.2.*

Requires : Wordpress (lmao)

Display photos from flickr wih your account id

## Description

Display a selected number of photos from flickr with the identifier of your account or that of a friend, you can also change the size of these images for display.

## Installation

1# Clone Repo

2# Open your wordpress admin panel

3# On the left menu, go to \"Plugin\" then \"add new\"

4# At the top of the page, click on \"Upload plugin\"

5# Choose the compressed file (flickr-api-widget.zip) and click on \"Install now\" then \"Activate Plugin\"

6# You should see the plugin \"Flickr API Widget\" in the list now

## Integration

1# On the left menu on panel admin, go to \"Apparence\" then \"Widgets\"

2# Normally it appears first in the list of available widgets

3# Select the widget and enjoy ;)
